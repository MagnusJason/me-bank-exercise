package com.magnusjason.mebankexercise

import assertk.all
import assertk.assertThat
import assertk.assertions.hasClass
import assertk.assertions.hasMessage
import assertk.assertions.isEqualTo
import assertk.assertions.isFailure
import java.math.BigDecimal
import java.time.LocalDateTime
import kotlin.test.Test

class MaterialisedLedgerTest {

    val simplePayment = TransactionRecord(
            "TX1",
            "ACC1",
            "ACC2",
            LocalDateTime.now(),
            BigDecimal.ONE,
            TransactionType.PAYMENT)

    @Test
    fun `cant add same transaction id more than once`() {
        val ledger = MaterialisedLedger()

        ledger.processTransaction(simplePayment)

        assertThat {
            ledger.processTransaction(simplePayment)
        }
                .isFailure().all {
                    hasMessage("Transaction ID already exists")
                    hasClass(MaterialisedLedger.LedgerException::class)
                }
    }

    @Test
    fun `transaction amount must be greater than zero`() {
        val ledger = MaterialisedLedger()

        assertThat {
            ledger.processTransaction(simplePayment.copy(amount = BigDecimal.ZERO))
        }
                .isFailure().all {
                    hasMessage("Transaction amount must be greater than zero")
                    hasClass(MaterialisedLedger.LedgerException::class)
                }
    }

    @Test
    fun `payment must be to different account`() {
        val ledger = MaterialisedLedger()

        assertThat {
            ledger.processTransaction(simplePayment.copy(
                    fromAccountId = "same",
                    toAccountId = "same"
            ))
        }
                .isFailure().all {
                    hasMessage("Cannot transfer money to the same account")
                    hasClass(MaterialisedLedger.LedgerException::class)
                }
    }

    @Test
    fun `reversal must have related transaction`() {
        val ledger = MaterialisedLedger()

        assertThat {
            ledger.processTransaction(simplePayment.copy(
                    transactionType = TransactionType.REVERSAL,
                    relatedTransaction = null
            ))
        }
                .isFailure().all {
                    hasMessage("Reversal has no related transaction")
                    hasClass(MaterialisedLedger.LedgerException::class)
                }
    }

    @Test
    fun `reversal must refer to existing transaction`() {
        val ledger = MaterialisedLedger()
        assertThat {
            ledger.processTransaction(simplePayment.copy(
                    transactionType = TransactionType.REVERSAL,
                    relatedTransaction = simplePayment.transactionId
            ))
        }
                .isFailure().all {
                    hasMessage("Cannot process reversal as, could not find related transaction")
                    hasClass(MaterialisedLedger.LedgerException::class)
                }
    }

    @Test
    fun `exercise sample integration test`() {
        val ledger = initialiseLedger(TransactionRecordFileParserTest.sampleInputFile)

        val (balance, numberOfTransactions) = ledger.relativeBalance(
                "ACC334455",
                LocalDateTime.parse("2018-10-20T12:00:00"),
                LocalDateTime.parse("2018-10-20T19:00:00"))

        assertThat(balance).isEqualTo(BigDecimal("-25.00"))
        assertThat(numberOfTransactions).isEqualTo(1)
    }

}
