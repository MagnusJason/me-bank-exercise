package com.magnusjason.mebankexercise

import assertk.assertThat
import assertk.assertions.isEqualTo
import java.math.BigDecimal
import java.nio.file.Paths
import java.time.LocalDateTime
import java.util.stream.Collectors
import kotlin.test.Test

class TransactionRecordFileParserTest {

    @Test
    fun `should parse sample file`() {
        val records = TransactionRecordFileParser().parseFile(sampleInputFile).collect(Collectors.toList())

        assertThat(records.size).isEqualTo(5)

        assertThat(records[0])
                .isEqualTo(TransactionRecord(
                        "TX10001",
                        "ACC334455",
                        "ACC778899",
                        LocalDateTime.parse("2018-10-20T12:47:55"),
                        BigDecimal("25.00"),
                        TransactionType.PAYMENT,
                        null))

        assertThat(records[3])
                .isEqualTo(TransactionRecord(
                        "TX10004",
                        "ACC334455",
                        "ACC998877",
                        LocalDateTime.parse("2018-10-20T19:45:00"),
                        BigDecimal("10.50"),
                        TransactionType.REVERSAL,
                        "TX10002"))
    }

    companion object {
        val sampleInputFile = Paths.get("src", "test", "resources", "sampleInput.txt")
    }


}
