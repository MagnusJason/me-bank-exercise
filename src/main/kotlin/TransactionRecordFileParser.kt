package com.magnusjason.mebankexercise

import java.math.BigDecimal
import java.nio.file.Files
import java.nio.file.Path
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.stream.Stream


class TransactionRecordFileParser {

    /**
     * This naive CSV parsing won't support many edge cases in the full RFC4180 spec,
     * but it's good enough for this exercise, given that the exercise specifies
     * that we may assume the input file is in a valid format.
     */
    fun parseLine(line: String): TransactionRecord {
        val split = line.split(",").map { it.trim() }
        return TransactionRecord(
                transactionId = split[0],
                fromAccountId = split[1],
                toAccountId = split[2],
                createdAt = LocalDateTime.parse(split[3], dateFormat),
                amount = BigDecimal(split[4]),
                transactionType = TransactionType.valueOf(split[5]),
                relatedTransaction = split.getOrNull(6)
        )
    }

    fun parseFile(path: Path) : Stream<TransactionRecord> {
        return Files.lines(path).map { parseLine(it) }
    }

    companion object {
        val dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")
    }

}

