package com.magnusjason.mebankexercise

enum class TransactionType {
    PAYMENT,
    REVERSAL
}