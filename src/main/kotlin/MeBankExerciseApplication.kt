package com.magnusjason.mebankexercise

import java.io.Console
import java.nio.file.Path
import java.nio.file.Paths
import java.time.LocalDateTime
import java.time.format.DateTimeParseException


fun main(args: Array<String>) {
    if (args.isEmpty()) {
        throw Exception("First argument must be path to input file")
    }
    val ledger = initialiseLedger(Paths.get(args[0]))
    val (accountId, from, to) = getBalanceQueryInteractively()
    val (relativeBalance, numberOfTransactions) = ledger.relativeBalance(accountId, from, to)
    println("Relative balance for the period is: $relativeBalance")
    println("Number of transactions included is: $numberOfTransactions")
}

fun getBalanceQueryInteractively() : Triple<String, LocalDateTime, LocalDateTime> {
    val console = System.console()?: throw Exception("Must run application from an interactive terminal")
    val accountId = console.readLine("accountId:\n")
    val from = readDate(console, "from:\n")
    val to = readDate(console, "to:\n")
    return Triple(accountId, from, to)
}

fun readDate(console: Console, prompt: String): LocalDateTime {
    while(true){
        try {
            val dateStr = console.readLine(prompt)
            return LocalDateTime.parse(dateStr, TransactionRecordFileParser.dateFormat)
        } catch (e: DateTimeParseException) {
            console.printf("Could not parse date, please check the format and try again\n")
        }
    }
}

fun initialiseLedger(transactionLog: Path): MaterialisedLedger {
    val ledger = MaterialisedLedger()
    TransactionRecordFileParser().parseFile(transactionLog).forEach { ledger.processTransaction(it) }
    return ledger
}
