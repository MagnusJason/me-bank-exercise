package com.magnusjason.mebankexercise

import java.math.BigDecimal
import java.time.LocalDateTime
import java.util.*

/**
 * A view of transaction records that removes original transactions when a corresponding reversal is seen
 */
class MaterialisedLedger {

    internal val transactionsByDate: NavigableMap<LocalDateTime, MutableList<TransactionRecord>> = TreeMap<LocalDateTime, MutableList<TransactionRecord>>()
    internal val transactionsById : MutableMap<String, TransactionRecord> = HashMap()

    fun processTransaction(transaction: TransactionRecord) {
        if (transactionsById.containsKey(transaction.transactionId)) {
            throw LedgerException("Transaction ID already exists")
        }
        if (transaction.amount <= BigDecimal.ZERO) {
            throw LedgerException("Transaction amount must be greater than zero")
        }
        when(transaction.transactionType) {
            TransactionType.PAYMENT -> processPayment(transaction)
            TransactionType.REVERSAL -> processReversal(transaction)
        }
    }

    private fun processPayment(transaction: TransactionRecord) {
        if (transaction.fromAccountId == transaction.toAccountId) {
            throw LedgerException("Cannot transfer money to the same account")
        }
        transactionsById[transaction.transactionId] = transaction
        transactionsByDate.merge(transaction.createdAt, mutableListOf(transaction)) { oldVal, newVal ->
            oldVal.addAll(newVal)
            oldVal
        }
    }

    private fun processReversal(transaction: TransactionRecord) {
        val relatedTransactionId = transaction.relatedTransaction ?: throw LedgerException("Reversal has no related transaction")
        if (!transactionsById.containsKey(transaction.relatedTransaction)) {
            throw LedgerException("Cannot process reversal as, could not find related transaction")
        }
        val originalTxn = transactionsById.remove(relatedTransactionId)!!
        val transactionsAtTime = transactionsByDate[originalTxn.createdAt]!!
        if(transactionsAtTime.size > 1) {
            transactionsAtTime.removeIf {
                it.transactionId == relatedTransactionId
            }
        } else {
            transactionsByDate.remove(originalTxn.createdAt)
        }
    }


    fun relativeBalance(accountId: String, fromInclusive: LocalDateTime, toExclusive: LocalDateTime) : BalanceAndTransactionCount {
        return transactionsByDate.subMap(fromInclusive, true, toExclusive, false).values.asSequence()
                .flatMap { it.asSequence() }
                .filter { it.fromAccountId == accountId || it.toAccountId == accountId }
                .map {
                    if (it.fromAccountId == accountId) {
                        it.amount.negate()
                    } else {
                        it.amount
                    }
                }
                .map { BalanceAndTransactionCount(it, 1) }
                .reduce { acc, next ->
                    BalanceAndTransactionCount(
                            acc.balance + next.balance,
                            acc.numberOfTransactions + next.numberOfTransactions)
                }
    }

    data class BalanceAndTransactionCount(val balance: BigDecimal, val numberOfTransactions: Int)
    class LedgerException(message: String) : Exception(message)

}