# ME bank exercise

This is an implementation of the ME Bank "Coding Challenge | D&I - API Team" exercise.  
This implementation was created on 2019-01-24, by Magnus Jason

## Building and running
This project is built with gradle, and includes a copy of the gradle wrapper.  
You may use `./gradlew` or `gradlew.bat` on unix-like systems and windows systems respectively.

First build the application distribution
```bash
gradle assembleDist
```

This will produce the directory `build/install/me-bank-exercise` which includes the application and library jars, 
as well as convenience scripts for unix-like systems and windows.  

You can then run the application via the script or with the java command
```bash
# Via script
build/install/me-bank-exercise/bin/me-bank-exercise src/test/resources/sampleInput.txt

# Via java command
java -cp "build/install/me-bank-exercise/lib/*" com.magnusjason.mebankexercise.MeBankExerciseApplicationKt src/test/resources/sampleInput.txt
```


## Discussion

### Design
There were at least a couple of ways to design a solution to this problem, could either optimise for memory use or query efficiency.
I chose to optimise for query efficiency.  
During the initialisation phase, we build a materialised view of the transaction ledger, that removes any reversed transactions.  
This is obviously faster than for each query, traversing the entire ledger tail, looking for any reversals.  

The transactions are indexed on two different axis, the time of the transaction and the transaction ID.  

The ID index is used for reversing transactions, and the time index is used for balance queries.  


Instead of removing transactions from the ledger, I considered instead, marking a payment as reversed.  
However for the purposes of this exercise, that would make things slightly more complicated with no real benefit.  
In a more complicated system, you would probably want to retain this information.  


### Tests
Test coverage could be improved in a number of areas, but I don't feel like spending much more time on it, as I have already spent more than a couple of hours on it.